<!doctype html>
<html lang="{{ app()->getLocale() }}" ea>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<meta name="csrf-token" content="{{ csrf_token() }}" />

	<link rel="icon" href="{{ asset('assets/images/favicon.ico') }}">

	<title>{{ config('app.name', 'Laravel') }}</title>
	<!-- CSS ================================================== -->
	<link rel="stylesheet" href="{{ asset('assets/css/bootstrap/bootstrap-theme.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/css/bootstrap/bootstrap.min.css') }}">
	<link rel="stylesheet" href="http://cdn.umbokc.com/ea/src/ea.css?v=1.3">
	<link rel="stylesheet" href="{{ asset('assets/css/base.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/css/skeleton.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/css/layout.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/css/colorbox.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/css/font-awesome.css') }} ">

	<!--[if lte IE 8]>
		<script src="{{ asset('assets/js/html5.js') }}"></script>
	<![endif]-->

</head>
<body ea>

	<div id="preloader">
		<div id="status">&nbsp;</div>
	</div>

	<section id="content">

		<article id="home" >
			<div class="logo">{{ config('app.name', 'Laravel') }}</div>
			<div class="home-text" ta:c>
				@yield('content')
			</div>
		</article>

	</section>

	<!-- JAVASCRIPT ================================================== -->
	<script src="{{ asset('assets/js/jquery.js') }}"></script>
	<script src="{{ asset('assets/js/modernizr.custom.js') }}"></script>
	<script src="{{ asset('assets/js/jquery.colorbox.js') }}"></script>
	<script src="{{ asset('assets/js/auth.js') }}"></script>

</body>
</html>
