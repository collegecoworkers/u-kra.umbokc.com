@extends('layouts.project')
@section('content')
<div pos:a t:0 l:0 p m m:h:big style="z-index: 111111111111;font-size: 18px">
	<a href="/" c#f td:n>&larr; Главная</a>
</div>
<div class="container">
	<div class="sixteen columns">
		<h1>{{ $item->title }}</h1> 
	</div>
	<div row w:100p>
		<div col=six p>
			<img src="{{ $item->getImage() }}" >
		</div>
		<div col=six p>
			<p><b>Номер: </b>{{ $item->number }}</p>
			@if ($item->cvv != '')
				<p><b>Cvv: </b>{{ $item->cvv }}</p>
			@endif
			@if ($item->date != '')
				<p><b>Дата: </b>{{ $item->date }}</p>
			@endif
			<p><b>Категория: </b>{{ $cat->title }}</p>
			<p><b>Описание: </b>{{ $item->desc }}</p>
		</div>
	</div>
	<div class="sixteen columns"> 
		<div class="separator-space"></div> 
	</div>	
	<div class="sixteen columns">
		<div class="link-project"><div class="cl-effect-8">
			<a href="{!! url('/edit', ['id'=>$item->id]); !!}"><span>Изменить</span></a>
			<a href="{!! url('/editimg', ['id'=>$item->id]); !!}"><span>Обновить изображение</span></a>
		</div></div>	
	</div>
</div>
 
@endsection
