@extends('layout.auth')

@section('content')
<div m:b:big>
	<h4>Вход</h4>
</div>
<div u-cont m:t:big>
	<form method="post" action="{{ route('login') }}">
		{{ csrf_field() }}
		<div class="form-group">
			<div class="input-group" m:h:a w:30p>
				<input type="text" class="form-control" name="email" id="email" required="" placeholder="E-mail" autocomplete="off" />
				@if ($errors->has('email'))
				<span class="help-block">
					<strong c#f>{{ $errors->first('email') }}</strong>
				</span>
				@endif
			</div>
		</div>

		<div class="form-group">
			<div class="input-group" m:h:a w:30p>
				<input required="" type="password" class="form-control" name="password" id="password" placeholder="Пароль" autocomplete="off" />
				@if ($errors->has('password'))
				<span class="help-block">
					<strong c#f>{{ $errors->first('password') }}</strong>
				</span>
				@endif
			</div>
		</div>

		<div class="form-group">
			<button type="submit" class="btn btn-success" w:10p>
				Войти
			</button>
		</div>
	</form>
	<div class="login-bottom-links">
		<a href="{{ route('register') }}" class="link" c#f>
			Зарегистрироваться
		</a>
	</div>
</div>
@endsection
