@extends('layouts.project')
@section('content')
<div pos:a t:0 l:0 p m m:h:big style="z-index: 111111111111;font-size: 18px">
	<a href="/card/{{ $item->id }}" c#f td:n>&larr; Карта</a>
</div>
<div class="container">
	<div class="sixteen columns">
		<h1>{{ $item->title }}</h1> 
	</div>
	{!! Form::open(array('url' => '/updateimg/' . $item->id, 'enctype'=>'multipart/form-data')) !!}
	<div class="form-group">
		<div class="input-group" m:h:a w:60p>
			<label p:b>Изображение</label>
			{!! Form::file('image', ['placeholder' => 'Изображение','class' => 'form-control']) !!}
			@if ($errors->has('image'))<span class="help-block"><strong c#f>{{ $errors->first('image') }}</strong></span>@endif
		</div>
	</div>
	<div class="form-group">
		<button type="submit" class="btn btn-success" w:20p d:b m:h:a>
			Обновить изображение
		</button>
	</div>

	{!! Form::close() !!}
</div>

@endsection

