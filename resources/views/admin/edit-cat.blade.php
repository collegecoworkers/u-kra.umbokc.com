@extends('../layouts.project')
@section('content')
<div pos:a t:0 l:0 p m m:h:big style="z-index: 111111111111;font-size: 18px">
	<a href="/" c#f td:n>&larr; Главная</a>
</div>
<div class="container">
	<div class="sixteen columns">
		<h1>{{ $item->title }}</h1> 
	</div>
	{!! Form::open(array('url' => '/update-cat/' . $item->id, 'enctype'=>'multipart/form-data')) !!}
	<div class="form-group">
		<div class="input-group" m:h:a w:60p>
			{!! Form::text('title', $item->title, ['placeholder' => 'Название','class' => 'form-control']) !!}
			@if ($errors->has('title')) <span class="help-block"><strong c#f>{{ $errors->first('title') }}</strong></span> @endif
		</div>
	</div>
	<div class="form-group">
		<div class="input-group" m:h:a w:60p>
			<textarea class="form-control" name="desc" placeholder="Описание" required autocomplete="off" >{{$item->desc}}</textarea>
			@if ($errors->has('desc'))<span class="help-block"><strong c#f>{{ $errors->first('desc') }}</strong></span>@endif
		</div>
	</div>
	<div class="form-group">
		<button type="submit" class="btn btn-success" w:20p d:b m:h:a>
			Обновить
		</button>
	</div>

	{!! Form::close() !!}
</div>

@endsection

