@extends('../layouts.project')
@section('content')
<div pos:a t:0 l:0 p m m:h:big style="z-index: 111111111111;font-size: 18px">
  <a href="/" c#f td:n>&larr; Главная</a>
</div>
<div class="container">
  <div class="sixteen columns">
    <h1>{{ $item->name }}</h1> 
  </div>
  {!! Form::open(array('url' => '/update-user/' . $item->id, 'enctype'=>'multipart/form-data')) !!}
  <div class="form-group">
    <div class="input-group" m:h:a w:60p>
      {!! Form::text('name', $item->name, ['placeholder' => 'Название','class' => 'form-control']) !!}
      @if ($errors->has('name')) <span class="help-block"><strong c#f>{{ $errors->first('name') }}</strong></span> @endif
    </div>
  </div>
  <div class="form-group">
    <div class="input-group" m:h:a w:60p>
      {!! Form::text('email', $item->email, ['placeholder' => 'Номер','class' => 'form-control']) !!}
      @if ($errors->has('email')) <span class="help-block"><strong c#f>{{ $errors->first('email') }}</strong></span> @endif
    </div>
  </div>
  <div class="form-group">
    <div class="input-group" m:h:a w:60p>
      {!! Form::select('is_admin', ['0' => 'Пользователь', '1' => 'Админ'], $item->is_admin, ['class' => 'form-control']) !!}
      @if ($errors->has('is_admin'))<span class="help-block"><strong c#f>{{ $errors->first('is_admin') }}</strong></span>@endif
    </div>
  </div>
  <div class="form-group">
    <button type="submit" class="btn btn-success" w:20p d:b m:h:a>
      Обновить
    </button>
  </div>

  {!! Form::close() !!}
</div>

@endsection

