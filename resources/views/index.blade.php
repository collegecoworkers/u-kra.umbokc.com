@extends('layouts.app')
@section('content')
<div id="preloader">
	<div id="status">&nbsp;</div>
</div>  

<!-- Primary Page Layout ================================================== -->

<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
<ul id="nav">
	<li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="icon-signout"></i></a></li>
	<li><a href="javascript:goTo('add');"><i class="icon-plus"></i></a></li>
	<li><a href="javascript:goTo('cards');"><i class="icon-laptop"></i></a></li>
	@if ($is_admin)
	<li><a href="javascript:goTo('all_cards');"><i class="icon-list"></i></a></li>
	<li><a href="javascript:goTo('all_users');"><i class="icon-user"></i></a></li>
	@endif
	<li><a href="javascript:goTo('home');"><i class="icon-home"></i></a></li>
</ul> 


<section id="content">
	<article id="home" class="active">
		<div class="logo">{{ config('app.name', 'Laravel') }}</div>
		<div class="home-text">
			<div u-cont>
				<h2>Храните карты вместе с нами</h2>
			</div>
		</div>
		<div class="link-home"><div class="cl-effect-8"><a href="javascript:goTo('cards');"><span>Мои карты</span></a></div></div>  
	</article>

	<article id="cards" class="off" p:t:big>
		<h1 p:t>Мои карты</h1>
		<div class="container">
			<div class="sixteen columns"> 
				<div class="sep"></div> 
			</div>
		</div>  
		<div class="clear"></div>
		<div class="container">
			<div class="sixteen columns">
				<div id="portfolio-filter">
					<ul id="filter">
						<li><div class="cl-effect-8"><a href="#" class="current" data-filter="*" title="">Все</a></div></li>
						@foreach ($cats as $item)
						<li><div class="cl-effect-8"><a href="#" data-filter="[cat='{{ $item->id }}']" title="">{{ $item->title }}</a></div></li>
						@endforeach
					</ul>
				</div>
			</div>
		</div> 
		<div class="clear"></div>
		<div class="container">
			<div class="all-works">
				@foreach ($cards as $item)
				<div class="one-third column one-work" cat='{{ $item->cat_id }}'> 
					<a class='' href="{!! url('/card', ['id'=>$item->id]); !!}" title="" >
						<div class="mask"></div>
						<div class="work-text"><h6>{{ $item->title }}</h6></div>  
						<img src="{{ $item->getImage() }}" h:200px objf:cov>
					</a>
				</div>
				@endforeach
			</div>
			<div class="sixteen columns"> 
				<div class="separator-space"></div> 
			</div>
			<div class="sixteen columns"> 
				<div class="link-box">
					<h6>Есть еще карты?</h6>
					<div class="cl-effect-8"><a href="javascript:goTo('add');"><span>Добавить</span></a></div>
				</div> 
			</div>
			<div class="sixteen columns"> 
				<div class="separator-space-bottom"></div> 
			</div>
		</div> 
		<div class="clear"></div>
	</article>
	@if ($is_admin)
	<article id="all_users" class="off" p:t:big>
		<h1 p:t>Пользователи</h1>
		<div class="container">
			<div class="sixteen columns"> 
				<div class="sep"></div> 
			</div>
		</div>  
		<div class="clear"></div>
		<div class="container">
			<table class="table">
				<thead>
					<tr>
						<td>#</td>
						<td>Имя</td>
						<td>Почта</td>
						<td>Это админ</td>
						<td>Действия</td>
					</tr>
				</thead>
				<tbody>
					@foreach ($users as $item)
						<tr>
							<td>{{$item->id}}</td>
							<td>{{$item->name}}</td>
							<td>{{$item->email}}</td>
							<td>{{$item->is_admin == 1 ? 'Да' : 'Нет' }}</td>
							<td>
								<a href="/edit-user/{{$item->id}}"><i class="icon-pencil action-icon"></i></a>
								<a href="/delete-user/{{$item->id}}" onclick="return confirm('Вы уверенны?')"><i class="icon-trash action-icon"></i></a>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div> 
		<div class="clear"></div>
	</article>

	<article id="all_cards" class="off" p:t:big>
		<h1 p:t>Карты</h1>
		<div class="container">
			<div class="sixteen columns"> 
				<div class="sep"></div> 
			</div>
		</div>  
		<div class="clear"></div>
		<div class="container">
			<h4 ta:l fl:l>Категории</h4>
			<div class="link-simple" d:b ta:r>
				<div class="cl-effect-8"><a href="/add-cat"><span>Добавить</span></a></div>
			</div>
			<table class="table">
				<thead>
					<tr>
						<td>#</td>
						<td>Название</td>
						<td>Действия</td>
					</tr>
				</thead>
				<tbody>
					@foreach ($cats as $item)
						<tr>
							<td>{{$item->id}}</td>
							<td>{{$item->title}}</td>
							<td>
								<a href="/edit-cat/{{$item->id}}"><i class="icon-pencil action-icon"></i></a>
								<a href="/delete-cat/{{$item->id}}" onclick="return confirm('Вы уверенны?')"><i class="icon-trash action-icon"></i></a>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div> 
		<div class="container">
			<h4 ta:l>Карты</h4>
			<table class="table">
				<thead>
					<tr>
						<td>#</td>
						<td>Название</td>
						<td>Пользователь</td>
						<td>Действия</td>
					</tr>
				</thead>
				<tbody>
					@foreach ($all_cards as $item)
						<tr>
							<td>{{$item->id}}</td>
							<td>{{$item->title}}</td>
							<td>{{ $users_arr[$item->user_id] }}</td>
							<td>
								<a href="/card/{{$item->id}}"><i class="icon-eye-open action-icon"></i></a>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div> 
		<div class="clear"></div>
	</article>
	@endif


	<article id="add" class="off" p:t:big>
		<h1 p:t>Добавить</h1>
		<div class="clear"></div>
		<div class="container" p:t:big>
			@include('layouts._form_card')
		</div> 
	</article>
</section>
@endsection
