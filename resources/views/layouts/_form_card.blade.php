{!! Form::open(array('url' => '/add', 'enctype'=>'multipart/form-data')) !!}
<div class="form-group">
	<div class="input-group" m:h:a w:60p>
		{!! Form::text('title', isset($model) ? $model->title : '', ['placeholder' => 'Название','class' => 'form-control']) !!}
		@if ($errors->has('title')) <span class="help-block"><strong c#f>{{ $errors->first('title') }}</strong></span> @endif
	</div>
</div>
<div class="form-group">
	<div class="input-group" m:h:a w:60p>
		{!! Form::text('number', isset($model) ? $model->number : '', ['placeholder' => 'Номер','class' => 'form-control']) !!}
		@if ($errors->has('number')) <span class="help-block"><strong c#f>{{ $errors->first('number') }}</strong></span> @endif
	</div>
</div>
<div class="form-group">
	<div class="input-group" m:h:a w:60p>
		{!! Form::text('cvv', isset($model) ? $model->cvv : '', ['placeholder' => 'Cvv (если есть)','class' => 'form-control']) !!}
		@if ($errors->has('cvv')) <span class="help-block"><strong c#f>{{ $errors->first('cvv') }}</strong></span> @endif
	</div>
</div>
<div class="form-group">
	<div class="input-group" m:h:a w:60p>
		{!! Form::text('date', isset($model) ? $model->date : '', ['placeholder' => 'Дата (если есть)','class' => 'form-control']) !!}
		@if ($errors->has('date')) <span class="help-block"><strong c#f>{{ $errors->first('date') }}</strong></span> @endif
	</div>
</div>
<div class="form-group">
	<div class="input-group" m:h:a w:60p>
		<label p:b>Изображение</label>
		{!! Form::file('image', ['placeholder' => 'Изображение','class' => 'form-control']) !!}
		@if ($errors->has('image'))<span class="help-block"><strong c#f>{{ $errors->first('image') }}</strong></span>@endif
	</div>
</div>
<div class="form-group">
	<div class="input-group" m:h:a w:60p>
		<label p:b>Категория</label>
		{!! Form::select('cat_id', $cats_arr, isset($model) ? $model->cat_id : '', ['class' => 'form-control']) !!}
		@if ($errors->has('cat_id'))<span class="help-block"><strong c#f>{{ $errors->first('cat_id') }}</strong></span>@endif
	</div>
</div>
<div class="form-group">
	<div class="input-group" m:h:a w:60p>
		<textarea class="form-control" name="desc" placeholder="Описание" required autocomplete="off" >{{isset($model) ? $model->desc : ''}}</textarea>
		@if ($errors->has('desc'))<span class="help-block"><strong c#f>{{ $errors->first('desc') }}</strong></span>@endif
	</div>
</div>
<div class="form-group">
	<button type="submit" class="btn btn-success" w:20p>
		Создать
	</button>
</div>

{!! Form::close() !!}
