<!DOCTYPE html>
<!--[if IE 8]><html class="no-js lt-ie9" lang="{{ app()->getLocale() }}"> <![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" lang="{{ app()->getLocale() }}"><!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name', 'Laravel') }}</title>

	<!-- CSS ================================================== -->
	<link rel="stylesheet" href="{{ asset('assets/css/bootstrap/bootstrap-theme.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/css/bootstrap/bootstrap.min.css') }}">
	<link rel="stylesheet" href="http://cdn.umbokc.com/ea/src/ea.css?v=1.3">
	<link rel="stylesheet" href="{{ asset('assets/css/base.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/css/skeleton.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/css/layout.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/css/colorbox.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/css/font-awesome.css') }} ">

	<!--[if lte IE 8]>
		<script src="{{ asset('assets/js/html5.js') }}"></script>
	<![endif]-->
</head>
<body ea>

	@yield('content')

	<!-- JAVASCRIPT ================================================== -->
	<script src="{{ asset('assets/js/jquery.js') }}"></script>
	<script src="{{ asset('assets/js/modernizr.custom.js') }}"></script>
	<script src="{{ asset('assets/js/jquery.colorbox.js') }}"></script>
	<script src="{{ asset('assets/js/mb.bgndGallery.js')}}"></script>
	<script src="{{ asset('assets/js/jquery.bxslider.min.js') }} "></script>
	<script src="{{ asset('assets/js/jquery.fitvids.js') }} "></script>
	<script src="{{ asset('assets/js/jquery.ferro.ferroMenu-1.0.min.js') }} "></script>
	<script src="{{ asset('assets/js/jquery.transit.min.js') }} "></script>
	<script src="{{ asset('assets/js/jquery.isotope.min.js') }} "></script>
	<script src="{{ asset('assets/js/jquery.masonry.min.js') }} "></script>
	<script src="{{ asset('assets/js/template.js') }} "></script>     
</body>
</html>
