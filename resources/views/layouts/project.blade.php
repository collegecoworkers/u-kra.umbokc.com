<!DOCTYPE html>
<!--[if IE 8]><html class="no-js lt-ie9" lang="{{ app()->getLocale() }}"> <![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" lang="{{ app()->getLocale() }}"><!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name', 'Laravel') }}</title>

	<!-- CSS ================================================== -->
	<link rel="stylesheet" href="{{ asset('assets/css/bootstrap/bootstrap-theme.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/css/bootstrap/bootstrap.min.css') }}">
	<link rel="stylesheet" href="http://cdn.umbokc.com/ea/src/ea.css?v=1.3">
	<link rel="stylesheet" href="{{ asset('assets/css/base.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/css/skeleton.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/css/project.css') }}">

	<!--[if lte IE 8]>
		<script src="{{ asset('assets/js/html5.js') }}"></script>
	<![endif]-->
</head>
<body ea>
	<div id="preloader">
		<div id="status">&nbsp;</div>
	</div>	

	<div id="project">
		@yield('content')
	</div>

	<!-- JAVASCRIPT ================================================== -->
	<script src="{{ asset('assets/js/jquery.js') }}"></script>
	<script src="{{ asset('assets/js/modernizr.custom.js') }}"></script>
	<script>
	//Preloader
	$(window).load(function() {
		$("#status").fadeOut();
		$("#preloader").delay(350).fadeOut("slow");
	})
	</script>
</body>
</html>
