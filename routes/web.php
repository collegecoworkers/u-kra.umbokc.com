<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'SiteController@Index');
Route::get('/card/{id}', 'SiteController@Card');
Route::get('/edit/{id}', 'SiteController@Edit');
Route::get('/editimg/{id}', 'SiteController@EditImg');

Route::post('/add', 'SiteController@Add');
Route::post('/update/{id}', 'SiteController@Update');
Route::post('/updateimg/{id}', 'SiteController@UpdateImg');



Route::get('/edit-user/{id}', 'AdminController@EditUser');
Route::get('/delete-user/{id}', 'AdminController@DeleteUser');
Route::post('/update-user/{id}', 'AdminController@UpdateUser');

Route::get('/add-cat', 'AdminController@AddCat');
Route::post('/create-cat', 'AdminController@CreateCat');
Route::get('/edit-cat/{id}', 'AdminController@EditCat');
Route::get('/delete-cat/{id}', 'AdminController@DeleteCat');
Route::post('/update-cat/{id}', 'AdminController@UpdateCat');

