//Preloader
$(window).load(function() {
	$("#status").fadeOut();
	$("#preloader").delay(350).fadeOut("slow");
}) 

/*global $:false */
$(function(){"use strict";
	$('.home-top').css({'height':($(window).height())+'px'});
	$(window).resize(function(){
		$('.home-top').css({'height':($(window).height())+'px'});
	});
});


//Navigation    

var currentIndex = 0;
var currentId = "home";
$(document).ready(function() {

});

var colors = {
	"home" : {
		"background" : "",
		"index" : 0
	},
	"about" : {
		"background" : "",
		"index" : 1
	},
	"services" : {
		"background" : "",
		"index" : 2
	},
	"folio" : {
		"background" : "",
		"index" : 3
	},
	"contact" : {
		"background" : "",
		"index" : 4
	}
};

function goTo(id){
	var obj = eval("colors."+id);
	
	$("body").css("background",obj.background);
	$("#ferromenu-controller,#nav li a").css("color",obj.background);
	if(obj.index > currentIndex){
		$(".active").addClass("off");
		$(".active").transition({
			x : -100,
			opacity : 0,
			zIndex : 0
		},600);
		
		$("#"+currentId).removeClass("active");
		
		$("#"+id).addClass("active");
		$("#"+id).transition({
			x : 400
		},0,function(){
			$("#"+id).removeClass("off");
			$("#"+id).transition({
				x : 0,
				opacity : 1,
				zIndex : 2
			},600);
		});
	}else if(obj.index < currentIndex){
		$(".active").addClass("off");
		$(".active").transition({
			x : 100,
			opacity : 0,
			zIndex : 0
		},600);
		$("#"+currentId).removeClass("active");

		
		$("#"+id).addClass("active");
		$("#"+id).transition({
			x : -400
		},0,function(){
			$("#"+id).removeClass("off");
			$("#"+id).transition({
				x : 0,
				opacity : 1,
				zIndex : 2
			},600);
		});
	}
	currentIndex = obj.index;
	currentId = id;
	
} 

//Home text rotator 

$(".rotator > div:gt(0)").hide();
setInterval(function() { 
	$('.rotator > div:first')
	.fadeOut(0)
	.next()
	.fadeIn(1000)
	.end()
	.appendTo('.rotator');
},  3000);

//Colorbox single project pop-up
$(document).ready(function(){
	$(".iframe").colorbox({iframe:true, width:"100%", height:"100%"});  
});

$(".group1").colorbox({rel:'group1'});    

/* DebouncedResize Function */
(function ($) { 
	var $event = $.event, 
	$special, 
	resizeTimeout;

	$special = $event.special.debouncedresize = { 
		setup : function () { 
			$(this).on('resize', $special.handler);
		}, 
		teardown : function () { 
			$(this).off('resize', $special.handler);
		}, 
		handler : function (event, execAsap) { 
			var context = this, 
			args = arguments, 
			dispatch = function () { 
				event.type = 'debouncedresize';
				
				$event.dispatch.apply(context, args);
			};

			if (resizeTimeout) {
				clearTimeout(resizeTimeout);
			}
			execAsap ? dispatch() : resizeTimeout = setTimeout(dispatch, $special.threshold);
		}, 
		threshold : 150 
	};
} )(jQuery);      


