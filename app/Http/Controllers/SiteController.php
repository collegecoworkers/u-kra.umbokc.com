<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{
	Cat,
	Card,
	User
};

class SiteController extends Controller
{

	public function __construct(){
		$this->middleware('auth');
	}

	public function Index() {
		$is_admin = auth()->user()->is_admin == 1;
		// $is_admin = true;

		$cats = Cat::all();
		$cards = Card::where('user_id', auth()->user()->id)->get();
		$cats_arr = []; foreach ($cats as $item) $cats_arr[$item->id] = $item->title;

		if ($is_admin) {
			$users = User::all();
			$users_arr = []; foreach ($users as $item) $users_arr[$item->id] = $item->name;
			$all_cards = Card::all();
			// dbg($all_cards);
		}
		return view('index')->with([
			'cats' => $cats, 'cats_arr' => $cats_arr, 'cards' => $cards,
			'users' => $is_admin ? $users : null,
			'all_cards' => $is_admin ? $all_cards : null,
			'users_arr' => $is_admin ? $users_arr : null,
			'is_admin' => $is_admin,
		]);
	}

	public function Card($id) {
		$card = Card::where('id', $id)->first();
		$cat = Cat::where('id', $card->cat_id)->first();
		return view('card')->with([
			'cat' => $cat,
			'item' => $card,
		]);
	}

	public function Edit($id) {
		$card = Card::where('id', $id)->first();
		$cats = Cat::all();
		$cats_arr = [];
		foreach ($cats as $item){
			$cats_arr[$item->id] = $item->title;
		}
		return view('edit')->with([
			'cats_arr' => $cats_arr,
			'item' => $card,
		]);
	}

	public function EditImg($id) {
		$card = Card::where('id', $id)->first();
		return view('editimg')->with([
			'item' => $card,
		]);
	}

	public function Add(Request $request) {

		$model = new Card();

		$model->title = request()->title;
		$model->number = request()->number;
		$model->cvv = (isset(request()->cvv) ? request()->cvv : '');
		$model->date = (isset(request()->date) ? request()->date : '');
		$model->cat_id = request()->cat_id;
		$model->desc = request()->desc;
		$model->user_id = auth()->user()->id;

		request()->validate(['image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',]);
		$imageName = time().'.'.request()->image->getClientOriginalExtension();
		request()->image->move(public_path('images'), $imageName);
		$model->image = $imageName;
		$model->save();

		return redirect('/');
	}

	public function Update($id, Request $request) {
		$model = Card::where('id', $id)->first();

		$model->title = request()->title;
		$model->number = request()->number;
		$model->cvv = (isset(request()->cvv) ? request()->cvv : '');
		$model->date = (isset(request()->date) ? request()->date : '');
		$model->cat_id = request()->cat_id;
		$model->desc = request()->desc;
		$model->user_id = auth()->user()->id;

		$model->save();
		return redirect()->to('/');
	}

	public function UpdateImg($id, Request $request) {
		$model = Card::where('id', $id)->first();

		request()->validate(['image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',]);
		$imageName = time().'.'.request()->image->getClientOriginalExtension();
		request()->image->move(public_path('images'), $imageName);
		$model->image = $imageName;
		$model->save();
		return redirect()->to('/');
	}
}
