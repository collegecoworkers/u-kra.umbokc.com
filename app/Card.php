<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
  protected $table = 'card';
  public $timestamps = false;

  public function getImage() {
    return ($this->image != '') ?
      '/images/' . $this->image :
      'http://placehold.it/500x500';
  }
}
